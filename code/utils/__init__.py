from fractions import gcd

def get_batch(train_size, test_size):
    batch_size = gcd(train_size, test_size)

    for i in range(1000,2):
        while(batch_size % i == 0 and batch_size >= 1000):
            batch_size = batch_size / i
    batch_size = 4
    return batch_size, train_size / batch_size, test_size / batch_size
