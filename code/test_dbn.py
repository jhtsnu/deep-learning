import sys
import os
import time
import numpy
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from dataset_loader import load_mnist
from layer import DBN
from utils import get_batch


def test_dbn(train_set, test_set, finetune_lr=0.1, pretraining_epochs=100,
             pretrain_lr=0.01, k=1, training_epochs=1000, hidden_layers=[1000]):
    """
    Demonstrates how to train and test a Deep Belief Network.

    This is demonstrated on MNIST.

    :type finetune_lr: float
    :param finetune_lr: learning rate used in the finetune stage
    :type pretraining_epochs: int
    :param pretraining_epochs: number of epoch to do pretraining
    :type pretrain_lr: float
    :param pretrain_lr: learning rate to be used during pre-training
    :type k: int
    :param k: number of Gibbs steps in CD/PCD
    :type training_epochs: int
    :param training_epochs: maximal number of iterations ot run the optimizer
    """

    train_set_x, train_set_y = train_set
    test_set_x, test_set_id = test_set
    # compute number of minibatches for training, validation and testing
    train_size = train_set_x.get_value(borrow=True).shape[0]
    test_size = test_set_x.get_value(borrow=True).shape[0]
    batch_size, n_train_batches, n_test_batches = get_batch(train_size, test_size)
    # numpy random generator
    rng = numpy.random.RandomState(123)
    print '... building the model'
    # construct the Deep Belief Network
    input_size = train_set_x.eval().size / train_set_y.eval().size
    dbn = DBN(numpy_rng=rng, n_ins=input_size,
              hidden_layers_sizes=hidden_layers,
              n_outs=10)

    #########################
    # PRETRAINING THE MODEL #
    #########################
    print '... getting the pretraining functions'
    pretraining_fns = dbn.pretraining_functions(train_set_x=train_set_x,
                                                batch_size=batch_size,
                                                k=k)

    print '... pre-training the model'
    start_time = time.clock()
    ## Pre-train layer-wise
    for i in xrange(dbn.n_layers):
        # go through pretraining epochs
        for epoch in xrange(pretraining_epochs):
            # go through the training set
            c = []
            for batch_index in xrange(n_train_batches):
                c.append(pretraining_fns[i](index=batch_index,
                                            lr=pretrain_lr))
            print 'Pre-training layer %i, epoch %d, cost ' % (i, epoch),
            print numpy.mean(c)

    end_time = time.clock()
    print >> sys.stderr, ('The pretraining code for file ' +
                          os.path.split(__file__)[1] +
                          ' ran for %.2fm' % ((end_time - start_time) / 60.))

    ########################
    # FINETUNING THE MODEL #
    ########################

    # get the training, validation and testing function for the model
    print '... getting the finetuning functions'
    index = T.lscalar('index')  # index to a [mini]batch
    tlearning_rate = theano.shared(finetune_lr)

    # compute the gradients with respect to the model parameters
    gparams = T.grad(dbn.finetune_cost, dbn.params)

    # compute list of fine-tuning updates
    updates = []
    for param, gparam in zip(dbn.params, gparams):
        updates.append((param, param - gparam * tlearning_rate))


    train_model = theano.function(
        inputs=[index],
        outputs=dbn.finetune_cost,
        updates=updates,
        givens={dbn.x: train_set_x[index * batch_size:
                                    (index + 1) * batch_size],
                dbn.y: train_set_y[index * batch_size:
                                    (index + 1) * batch_size]})

    test_model = theano.function(
        inputs = [index], outputs = dbn.y_pred,
        givens={dbn.x: test_set_x[index * batch_size:
                                   (index + 1) * batch_size]})

    fit_model = theano.function(
        [index], dbn.errors,
        givens={dbn.x: train_set_x[index * batch_size:
                                    (index + 1) * batch_size],
                dbn.y: train_set_y[index * batch_size:
                                    (index + 1) * batch_size]})

    best_params = None
    best_validation_loss = numpy.inf
    test_score = 0.
    start_time = time.clock()

    done_looping = False
    epoch = 0

    while (epoch < training_epochs) and (not done_looping):
        epoch = epoch + 1
        # tlearning_rate.set_value(finetune_lr/(1+epoch))

        for minibatch_index in xrange(n_train_batches):
            minibatch_avg_cost = train_model(minibatch_index)

        # test it on the test set
        fit_losses = [[fit_model(i) for i
                       in xrange(n_train_batches)]]
        fit_score = numpy.mean(fit_losses)
        print(('     epoch %i, test error of best model %f, learning rate %f  %%') %
              (epoch, fit_score * 100., tlearning_rate.eval()))

    print 'Create submission.csv...'

    #print [test_model(i) for i in xrange(n_test_batches)]
    prediction = numpy.concatenate(
        [[test_model(i) for i in xrange(n_test_batches)]]
        ).reshape(1, test_size)[0] #n_test_batches
    with open('submission.csv', 'w') as f:
        for id, pred in zip(test_set_id.eval(), prediction):
            f.write('%d,%d\n' % (id, pred))

    end_time = time.clock()

if __name__ == '__main__':
    train_set, test_set = load_mnist()
    test_dbn(train_set, test_set,
             finetune_lr=0.1, pretraining_epochs=3,
             pretrain_lr=0.01, k=1, training_epochs=10,
             hidden_layers=[10,10])
