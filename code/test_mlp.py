import time
import numpy
import theano
import theano.tensor as T

from dataset_loader import load_forest, load_mnist
from layer import MLP
from utils import get_batch

def test_mlp(train_set, test_set,
             learning_rate=0.01, L1_reg=0.00, L2_reg=0.0001, n_epochs=1000,
             n_hidden=500):
    """
    Demonstrate stochastic gradient descent optimization for a multilayer
    perceptron

    :type learning_rate: float
    :param learning_rate: learning rate used (factor for the stochastic
    gradient

    :type L1_reg: float
    :param L1_reg: L1-norm's weight when added to the cost (see
    regularization)

    :type L2_reg: float
    :param L2_reg: L2-norm's weight when added to the cost (see
    regularization)

    :type n_epochs: int
    :param n_epochs: maximal number of epochs to run the optimizer

   """
    train_set_x, train_set_y = train_set
    test_set_x, test_set_id = test_set
    # compute number of minibatches for training, validation and testing
    train_size = train_set_x.get_value(borrow=True).shape[0]
    test_size = test_set_x.get_value(borrow=True).shape[0]
    batch_size, n_train_batches, n_test_batches = get_batch(train_size, test_size)
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print '... building the model'

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    rng = numpy.random.RandomState(1234)

    input_size = train_set_x.eval().size / train_set_y.eval().size
    # construct the MLP class
    classifier = MLP(rng=rng, input=x, n_in= input_size,
                     n_hidden=n_hidden, n_out=10)

    # the cost we minimize during training is the negative log likelihood of
    # the model plus the regularization terms (L1 and L2); cost is expressed
    # here symbolically
    cost = classifier.negative_log_likelihood(y) \
         + L1_reg * classifier.L1 \
         + L2_reg * classifier.L2_sqr

    # compiling a Theano function that computes the mistakes that are made
    # by the model on a minibatch
    fit_model = theano.function(inputs=[index],
            outputs=classifier.errors(y),
            givens={
                x: train_set_x[index * batch_size :
                               (index + 1) * batch_size],
                y: train_set_y[index * batch_size :
                               (index + 1) * batch_size]})

    test_model = theano.function(inputs=[index],
            outputs=classifier.y_pred,
            givens={
                x: test_set_x[index * batch_size :
                              (index + 1) * batch_size]})

    # compute the gradient of cost with respect to theta (sotred in params)
    # the resulting gradients will be stored in a list gparams
    gparams = []
    for param in classifier.params:
        gparam = T.grad(cost, param)
        gparams.append(gparam)

    # specify how to update the parameters of the model as a list of
    # (variable, update expression) pairs
    updates = []
    # given two list the zip A = [a1, a2, a3, a4] and B = [b1, b2, b3, b4] of
    # same length, zip generates a list C of same size, where each element
    # is a pair formed from the two lists :
    #    C = [(a1, b1), (a2, b2), (a3, b3), (a4, b4)]
    tlearning_rate = theano.shared(learning_rate)
    for param, gparam in zip(classifier.params, gparams):
        updates.append((param, param - tlearning_rate * gparam))

    # compiling a Theano function `train_model` that returns the cost, but
    # in the same time updates the parameter of the model based on the rules
    # defined in `updates`
    train_model = theano.function(inputs=[index], outputs=cost,
            updates=updates,
            givens={
                x: train_set_x[index * batch_size:
                               (index + 1) * batch_size],
                y: train_set_y[index * batch_size:
                               (index + 1) * batch_size]})


    ###############
    # TRAIN MODEL #
    ###############
    print '... training'

    # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    validation_frequency = min(n_train_batches, patience / 2)
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_params = None
    best_validation_loss = numpy.inf
    best_iter = 0
    fit_score = 0.
    start_time = time.clock()

    epoch = 0
    done_looping = False

    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        # tlearning_rate.set_value(learning_rate/(1+epoch))

        for minibatch_index in xrange(n_train_batches):
            minibatch_avg_cost = train_model(minibatch_index)
            # iteration number
        # test it on the test set
        fit_losses = [fit_model(i) for i
                       in xrange(n_train_batches)]
        fit_score = numpy.mean(fit_losses)

        print(('     epoch %i, test error of best model %f, learning rate %f  %%') %
              (epoch, fit_score * 100., tlearning_rate.eval()))

    print 'Create submission.csv...'

    out = open('output.csv', 'wb')
    prediction = numpy.concatenate(
        [test_model(i) for i in xrange(n_test_batches)])
    with open('submission.csv', 'w') as f:
        for id, pred in zip(test_set_id.eval(), prediction):
            f.write('%d,%d\n' % (id, pred))

    end_time = time.clock()


if __name__ == '__main__':
    train_set, test_set = load_forest()
    test_mlp(train_set, test_set,
             learning_rate=0.01, L1_reg=0.00, L2_reg=0.00, n_epochs=10000,
             n_hidden=150)
