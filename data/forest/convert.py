import numpy
import gzip
import theano
import cPickle
import urllib
import os

def get_labeled_data(filename):
    data = numpy.genfromtxt(filename, dtype=theano.config.floatX,
                            delimiter=',', skip_header=1)
    return (numpy.delete(numpy.delete(data, -1, 1), 0, 1), numpy.int32(data[:,-1]))

def get_unlabeled_data(filename):
    data = numpy.genfromtxt(filename, dtype=theano.config.floatX,
                            delimiter=',', skip_header=1)
    return (numpy.delete(data, 0, 1), numpy.int32(data[:,0]))

if __name__ == '__main__':

    print 'Converting csv file to gzip of narray file...'

    train_data_x, train_data_y = get_labeled_data('train.csv')
    test_data_x, test_data_id = get_unlabeled_data('test.csv')

    print 'Data serializing....'

    results = ((train_data_x, train_data_y), (test_data_x, test_data_id))
    f_pkl = open('forest.pkl', 'w')
    cPickle.dump(results, f_pkl)
    f_pkl.close()

    print 'Compressing....'

    f_pkl = open('forest.pkl', 'r')
    f = gzip.open('forest.pkl.gz', 'wb')
    f.writelines(f_pkl)
    f_pkl.close()
    f.close()

    print 'Done.'
