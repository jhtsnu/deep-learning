See http://www.kaggle.com/c/forest-cover-type-prediction

USAGE
=====

1. Place 'train.csv' and 'test.csv' file to data/forest directory
2. Run convert.py then 'forest.pkl.gz' file will generated
3. now, enjoy it.

Or just download http://a.bl.ee/static/machine-learning/forest.pkl.gz

HOW TO OPEN serialized gzip file
================================

    import cPickle, gzip
    f = gzip.open('forest.pkl.gz', 'rb')
    train_data, test_data = cPickle.load(f)
